import './App.css';
import Words from "./pages/words";

function App() {
  return (
    <div className="App">
      <Words></Words>
    </div>
  );
}

export default App;

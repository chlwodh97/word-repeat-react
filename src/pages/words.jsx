import React, {useState} from 'react';


const Words = () => {
  const [userInput, setUserInput] = useState('');
  const [words, setWords] = useState([]);


  const handleUserInputChange = (e) => {
    setUserInput(e.target.value);
  }



    const handleSubmit = (e) => {
        // 폼안에 다른 이벤트가 많기 때문에 다른 이벤트를 막아야함 -> 이벤트 다 기본값으로 돌려 놓기
        e.preventDefault();


        if (words.length > 0 && words[words.length - 1] === words[0]) {
            alert("똑같은 단어 금지")
        } else if(words.length > 0 ) {
            words.push(userInput)
            setWords(words)
        }
    }
  return (
      <div>
        <h2>끝말잇기</h2>
          {/* 폼 -> 하나로 만들기 (묶기) onclick 없이 onSubmit 가능*/}
          {/* 제출이라는 행위가 일어나면 submit 을 실행을 하겠다 엔터치면 이벤트가 넘어가서 실행가능 -> onClick 은반드시 클릭 */}
          <form onSubmit={handleSubmit}>
            <input value={userInput} onChange={handleUserInputChange}/>
            <button type="submit">입력</button>
          </form>
        <div>
            {words.map((item, index) => (
                <p key={index}>
                    {index}.{item}
                </p>
            ))}
        </div>
      </div>
  )
}

export default Words;